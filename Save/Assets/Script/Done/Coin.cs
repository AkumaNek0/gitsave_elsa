using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Coin : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") == true)
        {
            Inventory.Instance.AddCoin();
            gameObject.SetActive(false);

            PlayerPrefs.SetInt("Coin level", 0);
        }
    }
}
