using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{ //Add respawn mechanic
public GameObject Player;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") == true)
        {
            Inventory.Instance.AddDeath();
            Player.transform.position = new Vector2(-7f, -3.25f);
        }
    }
}
