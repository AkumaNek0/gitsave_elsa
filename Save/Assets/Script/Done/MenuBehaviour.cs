using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuBehaviour : MonoBehaviour
{
    public GameObject MenuVisible;
    public GameObject GameHub;
    public GameObject Level1;
    public int PlayerLevel = 1;
    public GameObject Player;
    public GameObject Level2;
    public Button Button2;
    public GameObject Level3;
    public Button Button3;
    
    private void Awake()
    {
        MenuVisible.gameObject.SetActive(true);
        if(PlayerPrefs.HasKey("Player level"))
        {
            PlayerLevel = PlayerPrefs.GetInt("Player level");
        }
        else {
            PlayerPrefs.SetInt("Player level", 1);
            PlayerLevel = PlayerPrefs.GetInt("Player level");
        }

        /*if(PlayerPrefs.HasKey("Coin level"))
        {
            Inventory.Instance.coin = PlayerPrefs.GetInt("Coin level");
        }
        else {
            PlayerPrefs.SetInt("Coin level", 0);
        }

        if(PlayerPrefs.HasKey("Death level"))
        {
            Inventory.Instance.death = PlayerPrefs.GetInt("Death level");
        }
        else {
            PlayerPrefs.SetInt("Death level", 0);
        }*/
    }
    void Update()
    {
        if(PlayerLevel == 2)
        {
            Button2.interactable = true;
        }
        else if(PlayerLevel == 3)
        {
            Button2.interactable = true;
            Button3.interactable = true;
        } else {}
    }
    public void Start1()
    {
        MenuVisible.gameObject.SetActive(false);
        Level1.gameObject.SetActive(true);
        GameHub.gameObject.SetActive(true);
        Player.gameObject.SetActive(true);
        Player.transform.position = new Vector2(-7f, -3.25f);
    }
    public void Start2()
    {
        MenuVisible.gameObject.SetActive(false);
        Level2.gameObject.SetActive(true);
        GameHub.gameObject.SetActive(true);
        Player.gameObject.SetActive(true);
        Player.transform.position = new Vector2(-4f, 0.25f);
    }
    public void Start3()
    {
        MenuVisible.gameObject.SetActive(false);
        Level3.gameObject.SetActive(true);
        GameHub.gameObject.SetActive(true);
        Player.gameObject.SetActive(true);
        Player.transform.position = new Vector2(0f, 0f);
    }
    public void BackToMenu()
    {
        MenuVisible.gameObject.SetActive(true);
        Level1.gameObject.SetActive(false);
        Level2.gameObject.SetActive(false);
        Level3.gameObject.SetActive(false);
        GameHub.gameObject.SetActive(false);
        Player.gameObject.SetActive(false);
    }
    public void EraseSave()
    {
        Debug.Log("Reset");
        PlayerPrefs.SetInt("Player level", 1);
        PlayerPrefs.SetInt("Coin level", 0);
        PlayerPrefs.SetInt("Death level", 0);
    }
}
