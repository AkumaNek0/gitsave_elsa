using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public static Inventory Instance;
    public int coin;
    public int death;
    public Text CoinNum;
    public Text DeathNum;
    public void Awake()
    {
        if(Instance)
        {
            Destroy(gameObject);
        }
        else{
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        if(PlayerPrefs.HasKey("Coin level"))
        {
            coin = PlayerPrefs.GetInt("Coin level");
        }
        else {
            PlayerPrefs.SetInt("Coin level", 0);
        }

        if(PlayerPrefs.HasKey("Death level"))
        {
            death = PlayerPrefs.GetInt("Death level");
        }
        else {
            PlayerPrefs.SetInt("Death level", 0);
        }
    }

    public void AddCoin()
    {
        coin++;
        Debug.Log("Number of coins : " + coin.ToString());
        CoinNum.text = coin.ToString();
        PlayerPrefs.SetInt("Coin level", coin);
    }
    public void AddDeath()
    {
        death++;
        Debug.Log("Number of deaths : " + death.ToString());
        DeathNum.text = death.ToString();
        PlayerPrefs.SetInt("Death level", death);
    }
}
