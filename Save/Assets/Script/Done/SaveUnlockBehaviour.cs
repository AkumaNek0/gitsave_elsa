using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveUnlockBehaviour : MonoBehaviour
{
    public MenuBehaviour menuBehaviour;
    public Collider2D DoorCollider;
    public GameObject Player;
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("Bump");
            menuBehaviour.Player.gameObject.SetActive(false);
            menuBehaviour.MenuVisible.gameObject.SetActive(true);
            menuBehaviour.Level1.gameObject.SetActive(false);
            menuBehaviour.Level2.gameObject.SetActive(false);
            menuBehaviour.Level3.gameObject.SetActive(false);
            menuBehaviour.GameHub.gameObject.SetActive(false);
            
            if(menuBehaviour.PlayerLevel < 2)
            {
                menuBehaviour.PlayerLevel++;
                Debug.Log("Level Up --> " + menuBehaviour.PlayerLevel);
            } 
            else if(menuBehaviour.PlayerLevel < 3 && menuBehaviour.Button2.interactable == true)
            {
                menuBehaviour.PlayerLevel++;
                Debug.Log("Level Up --> " + menuBehaviour.PlayerLevel);
            } else {}

            PlayerPrefs.SetInt("Player level", menuBehaviour.PlayerLevel);
        }
    }
    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
